<?php
session_start();
if (!isset($_SESSION["CONNECTE"]) && $_SESSION["CONNECTE"] !== "YES") {
    header("Location: ./login.php");
}
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>
    <?php
    echo "Identifiant utilisateur connecté : " . $_SESSION['login'] . "<br>"
    ?>
    <a href="./deconnexion.php"><button>Deconnexion</button></a>
</body>

</html>
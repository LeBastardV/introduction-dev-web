<?php
session_start();

if (isset($_SESSION["CONNECTE"])) {
    unset($_SESSION["CONNECTE"]);
    unset($_SESSION["login"]);
}
header("Location: ./login.php");

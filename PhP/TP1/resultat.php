<?php
echo "<h1> Résultat du Test </h1>";
//Setting $_POST Variable
$comparatorOne = $_POST['comparator-one'];
$comparatorTwo = $_POST['comparator-two'];
$compararedNumber = $_POST['compared-number'];

if ($comparatorOne < $comparatorTwo) {
    $leftComparator = $comparatorOne;
    $rightComparator = $comparatorTwo;
} else {
    $rightComparator = $comparatorOne;
    $leftComparator = $comparatorTwo;
}
//Test if is include
$isInclude = ($leftComparator <= $compararedNumber && $rightComparator >= $compararedNumber) ? True : False;

//Display result
if ($isInclude) {
    echo "Oui, " . $compararedNumber . " est compris entre " . $leftComparator . " et " . $rightComparator;
} else {
    echo "Non, " . $compararedNumber . " n'est pas compris entre " . $leftComparator . " et " . $rightComparator;
}

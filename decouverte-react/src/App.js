import React, { useEffect, useState } from 'react';
import './App.css';

function App() {
  const [isClicked, setIsClicked] = useState(false);
  const [items, setItems] = useState([]);

  const Card = (props) => {
    return (
      <div>
        <h1> {props.items} </h1>
        <p> {props.items} </p>
      </div>
    )
  }

  useEffect(() => {
    if (isClicked) {
      var department = document.getElementById("select-department").value
      var administrationType = document.getElementById("select-administration-type").value
      var url = "https://etablissements-publics.api.gouv.fr/v3/departements/" + department + "/" + administrationType
      fetch(url)
        .then(result => result.json())
        .then(json => setItems(json.features))
        .catch(error => console.log(error))
    }
    setIsClicked(false)
  }, [isClicked]);

  useEffect(() => {
    var div = document.getElementById("result-area")
    div.innerHTML = ""
    if (items.lenght != 0) {

      for (let i = 0; i < items.length; i++) {
        console.log(items[i])
        var div2 = document.createElement('div');
        div2.setAttribute("id", items[i].properties.id);
        div.appendChild(div2)
        document.getElementById(items[i].properties.id).innerHTML = items[i].properties.nom
      }
    }
    setIsClicked(false)
  }, [items]);


  return (
    <div className="App">
      <h1>Trouver son administration en Normandie</h1>

      <select id="select-department">
        <option value="14">Calvados</option>
        <option value="27">Eure</option>
        <option value="50">Manche</option>
        <option value="61">Orne</option>
        <option value="76">Seine Maritine</option>
      </select>
      <select id="select-administration-type">
        <option value="cpam">Caisse Primaire d'Assurance Maladie</option>
        <option value="ars">Agence Regionale de Santé</option>
        <option value="tresorerie">Trésorerie</option>
        <option value="urssaf">URSSAF</option>
        <option value="tribunal_commerce">Tribunal de Commerce</option>
      </select>
      <button name="search-administration" onClick={() => setIsClicked(true)}>
        Rechercher une administration</button>
      <button name="clear-research" onClick={() => setItems([])}>Vider la recherche</button>


      <div id="result-area">
      </div>
    </div>
  );
}

export default App;

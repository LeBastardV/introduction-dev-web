// Small function to copy text from input to other input in same form
function copieTextMemeFormulaire() {
    document.getElementById("textRetourForm1-input").value = document.getElementById("text-input").value
}
// Small function to copy select value into input in same form
function copieSelect() {
    document.getElementById("textRetourForm1-input").value = document.getElementById("pays-select").value
}
// Small function to copy text from input to other input in other form
function copieAutreFormulaire(){
    document.getElementById("textRetourForm2-input").value = document.getElementById("text-input").value
}
// Function to validate "qcm" answer
function validationqcm() {
    var radios = document.getElementsByName("q1");
    var goodAnswer = 0;
    for(var i = 0; i < radios.length; i++){
        if(radios[i].checked && radios[i].value == "leve"){
            goodAnswer +=1;
        }
    }
    var radios = document.getElementsByName("q2");
    for(var i = 0; i < radios.length; i++){
        if(radios[i].checked && radios[i].value == "Arrete"){
            goodAnswer +=1;
        }
    }
    var radios = document.getElementsByName("q3");
    for(var i = 0; i < radios.length; i++){
        if(radios[i].checked && radios[i].value == "Reprendre"){
            goodAnswer +=1;
        }
    }
    document.getElementById("reponseQCM").innerHTML = goodAnswer + " Réponses bonnes"
}